﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;

public class DialogueSys : MonoBehaviour
{

    public TextMeshProUGUI textDisplay;
    [TextArea(3, 10)]
    public string[] sentences;
    [SerializeField]
    private int index;
    public float typingSpeed;
    public bool dialStart;
    private bool playerOnRange;
    [SerializeField]
    private Canvas dialCanvas;

    //[SerializeField]
    //private Image buttonOverlay;
    [SerializeField]
    private Image textBox;

    [SerializeField]
    private AudioSource textScroll;

    public PlayerMovement playerMovement;
    public InputManager inputManager;
    private int sentenceOriginal;


    void Start()
    {
        playerMovement = GameObject.Find("Player").GetComponent<PlayerMovement>();
        dialCanvas.enabled = false;
        StartCoroutine(Type());
        textBox.enabled = false;
        textDisplay.enabled = false;
    }

    void Awake()
    {
        inputManager = new InputManager();
        inputManager.Player.Interact.performed += ctx => Interact();
        sentenceOriginal = sentences.Length;
    }

    void Update()
    {

        Debug.Log(index);
    }

    void Interact()
    {
        if (playerOnRange)
        {
            if (!dialStart && index == 0)
            {
                dialCanvas.enabled = true;
                dialStart = true;
                textBox.enabled = true;
                textDisplay.enabled = true;
                playerMovement.canMove = false;
            }
            else if (dialStart && textDisplay.text == sentences[index])
                NextSentence();
        }
    }

    IEnumerator Type()
    {
        foreach (char letter in sentences[index].ToCharArray())
        {
            textDisplay.text += letter;
            yield return new WaitForSeconds(typingSpeed);
        }
    }

    public void NextSentence()
    {
        if (index < sentences.Length - 1)
        {
            textScroll.Play();
            index++;
            textDisplay.text = "";
            StartCoroutine(Type());
        }
        else
        {
            playerMovement.canMove = true;
            textScroll.Stop();
            textDisplay.text = "";
            dialStart = false;
            textDisplay.enabled = false;
            textBox.enabled = false;
            index = 0;
            StartCoroutine(Type());
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            playerOnRange = true;
            Debug.Log("range dialogo");
            //buttonOverlay.enabled = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            playerOnRange = false;
            //buttonOverlay.enabled = false;
            index = 0;

        }
    }

    private void OnEnable()
    {
        inputManager.Enable();
    }

    private void OnDisable()
    {
        inputManager.Disable();
    }
}

