﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemObject : ScriptableObject
{
    public GameObject prefab;
    public string itemName;
    [TextArea(15, 20)]
    public string itemDescription;
    public MeshFilter itemMesh;
}
