﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory System/Inventory")]
public class InventoryObject : ScriptableObject
{
    public List<InventorySlot> Container = new List<InventorySlot>();
    public void AddItem(ItemObject _item, int _amount, string _name, MeshFilter _mesh)
    {
        bool hasItem = false;

        for (int i = 0; i < Container.Count; i++)
        {
            if (Container[i].item == _item)
            {
                Container[i].AddAmount(_amount);
                hasItem = true;
                break;
            }
        }

        if (!hasItem)
        {
            Container.Add(new InventorySlot(_item, _amount, _name, _mesh));
        }

    }

}

[System.Serializable]
public class InventorySlot
{
    public ItemObject item;
    public int amount;
    public string name;
    public MeshFilter mesh;

    public InventorySlot(ItemObject _item, int _amount, string _name, MeshFilter _mesh)
    {
        item = _item;
        amount = _amount;
        name = _name;
        mesh = _mesh;
        
    }
    public void AddAmount(int value)
    {
        amount += value;
    }
}