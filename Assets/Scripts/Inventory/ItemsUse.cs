﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemsUse : MonoBehaviour
{
    public DisplayInventory displayInventory;

    // Start is called before the first frame update
    void Start()
    {
        displayInventory = this.GetComponent<DisplayInventory>();        
    }

    public void UseItem()
    {
        if (displayInventory.inventoryOn)
        {
            switch (displayInventory.currentItemName) //TODO UPDATE LIST
            {
                case "Test KEY":
                    Debug.Log("Used key");
                    break;

                case "duck":
                    Debug.Log("used honk");
                    break;
            }
        }

    }
}
