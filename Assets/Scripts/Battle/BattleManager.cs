﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum BattleState { START, PLAYERTURN, ENEMYTURN, WIN, LOSE }

public class BattleManager : MonoBehaviour
{
    public BattleState state;

    public GameObject playerBattle;
    public GameObject enemyBattle;

    //TODO GET ACTORS AUTOMATICALLY FROM NORMAL SCENE
    public Transform playerLocation;
    public Transform enemyLocation;

    private UnitScript playerBattleUnit;
    private UnitScript enemyBattleUnit;

    public TextMeshProUGUI dialogueText;

    public BattleHUD playerHUD;
    public BattleHUD enemyHUD;

    void Start()
    {
        state = BattleState.START;
        StartCoroutine (StartBattle());
    }

    IEnumerator StartBattle()
    {
        GameObject playerObj = Instantiate(playerBattle, playerLocation);
        playerBattleUnit = playerObj.GetComponent<UnitScript>(); //get stats

        GameObject enemyObj = Instantiate(enemyBattle, enemyLocation);
        enemyBattleUnit = enemyObj.GetComponent<UnitScript>();

        dialogueText.text = enemyBattleUnit.unitName + " approaches you."; //get name from unitscript

        playerHUD.SetHUD(playerBattleUnit);
        enemyHUD.SetHUD(enemyBattleUnit);

        Debug.Log("battle started");
        yield return new WaitForSeconds(2f);

        if (playerBattleUnit.baseSpeed > enemyBattleUnit.baseSpeed)
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }
        }

    IEnumerator PlayerAttack()
    {
        bool isDead = enemyBattleUnit.TakeDamage(playerBattleUnit.DmgCalculation(out int calcDmg));
        enemyHUD.HPUpdate(enemyBattleUnit.currentHP);
        dialogueText.text = "You deal damage to " + enemyBattleUnit.name;
        yield return new WaitForSeconds(2f);

        if (isDead)
        {
            state = BattleState.WIN;
            BattleEnd();
        }
        else
        {
            state = BattleState.ENEMYTURN;
            StartCoroutine(EnemyTurn());
        }

    }

    void BattleEnd()
    {
        if (state == BattleState.WIN)
        {
            dialogueText.text = "You made it!";
            playerBattleUnit.currentEXP += enemyBattleUnit.givenEXP;
            playerBattleUnit.LevelUp();
        }
        else if (state == BattleState.LOSE)
            dialogueText.text = "You died thank you forever.";
    }

    void PlayerTurn()
    {
        dialogueText.text = "What do you want to do?";
    }

    IEnumerator EnemyTurn() //TODO AI
    {
        dialogueText.text = enemyBattleUnit.unitName + " strikes you.";
        yield return new WaitForSeconds(1f);

        bool isDead = playerBattleUnit.TakeDamage(enemyBattleUnit.DmgCalculation(out int calcDmg));
        playerHUD.HPUpdate(playerBattleUnit.currentHP);
        yield return new WaitForSeconds(1f);

        if (isDead)
        {
            state = BattleState.LOSE;
            BattleEnd();
        }
        else
        {
            state = BattleState.PLAYERTURN;
            PlayerTurn();
        }
    }

    public void OnAttackButton()
    {
        if (state != BattleState.PLAYERTURN)
            return;
        StartCoroutine(PlayerAttack());
    }
}
