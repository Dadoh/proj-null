﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class BattleHUD : MonoBehaviour
{
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI lvText;
    public Slider hpBar;

    public void SetHUD(UnitScript unit)
    {
        nameText.text = unit.unitName;
        lvText.text = "Lvl " + unit.unitLevel;
        hpBar.maxValue = unit.vitality;
        hpBar.value = unit.currentHP;
    }

    public void HPUpdate(int hp)
    {
        hpBar.value = hp;

    }
}
