﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitScript : MonoBehaviour
{
    public string unitName;

    [Header("Stats")]
    public int unitLevel;
    public int baseAtk;
    public int vitality;
    public int currentHP;
    public int baseSpeed;
    public int baseDef;
    public int currentEXP;
    public int givenEXP;
    private int necessaryEXP;

    [Header("Moods")]
    public int rage;
    public int hollow;
    public int happy;
    public int empathy;
    public int reflexes;

    public int AtkCalculation(out int atk)
    {
        try
        {
            atk = baseAtk - (hollow / 2) + (happy / 2) - (empathy / 3);
            return atk;
        }
        catch (System.DivideByZeroException)
        {
            Debug.LogError("Tried to divide by 0");
            atk = baseAtk;
            return atk;
        }
    }

    public int DefCalculation(out int def)
    {
        try
        {
            def = baseDef - happy + (hollow / 2);
            return def;
        }
        catch(System.DivideByZeroException)
        {
            Debug.LogError("Tried to divide by 0");
            def = baseDef;
            return def;
        }
    }

    public int SpeedCalculation(out int speed) //TODO implement in battlemanager
    {
        try
        {
            speed = baseSpeed + (reflexes / 2);
            return speed;
        }
        catch(System.DivideByZeroException)
        {
            Debug.LogError("Tried to divide by 0");
            speed = baseSpeed;
            return speed;
        }
    }

    public int DmgCalculation(out int calcDmg)
    {
        //todo move bp && enemy def
        //((2lvl/5)*atk*bp)/def)/50)+2)*rnd(150o200)/20
        try
        {
            calcDmg = ((((((2 * unitLevel / 5) * AtkCalculation(out int atk)) / DefCalculation(out int def)) / 50) + 2) * Random.Range(150, 200)) / 20;
            return calcDmg;
        }
        catch(System.DivideByZeroException)
        {
            Debug.LogError("Tried to divide by 0");
            calcDmg = 1;
            return calcDmg;
        }
    }



    public bool TakeDamage(/*int dmg*/int calcDmg)
    {
        
        //currentHP -= dmg;
        currentHP -= DmgCalculation(out calcDmg);
        if (currentHP <= 0)
            return true;
        else
            return false;

    }

    public void LevelUp()
    {
        necessaryEXP = (int)Mathf.Pow(unitLevel, 3f);
        if (currentEXP >= necessaryEXP)
            unitLevel++;
    }



}
