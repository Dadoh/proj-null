﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; //CRITICAL

public class PlayerMovement : MonoBehaviour
{
    public float speed;

    public InputManager inputManager;
    private Vector2 movementInput;
    private Vector3 inputDirection;
    private Vector3 moveVector;
    private Quaternion currentRotation;
    public bool canMove;

    private void Awake()
    {
        inputManager = new InputManager();
        
        inputManager.Player.Move.performed += ctx => movementInput = ctx.ReadValue<Vector2>();
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            float h = movementInput.x;
            float v = movementInput.y;

            Vector3 targetInput = new Vector3(h, 0, v);
            inputDirection = Vector3.Lerp(inputDirection, targetInput, Time.deltaTime * speed);

            Vector3 camForward = Camera.main.transform.forward;
            Vector3 camRight = Camera.main.transform.right;

            camForward.y = 0f;
            camRight.y = 0f;

            Vector3 desiredDirection = camForward * inputDirection.z + camRight * inputDirection.x;

            Move(desiredDirection);
            Turn(desiredDirection);
        }
    }

    private void Move(Vector3 desiredDirection)
    {
        moveVector.Set(desiredDirection.x, 0f, desiredDirection.z);
        moveVector = moveVector * speed * Time.deltaTime;

        transform.position += moveVector;
    }

    private void Turn(Vector3 desiredDirection)
    {
        if ((desiredDirection.x > 0.1 || desiredDirection.x < -0.1) || (desiredDirection.z > 0.1 || desiredDirection.z < -0.1))
        {
            currentRotation = Quaternion.LookRotation(desiredDirection);
            transform.rotation = currentRotation;
        }
        else
        {
            transform.rotation = currentRotation;
        }
    }


    private void OnEnable()
    {
        inputManager.Enable();
        canMove = true;
    }

    private void OnDisable()
    {
        inputManager.Disable();
    }
}
