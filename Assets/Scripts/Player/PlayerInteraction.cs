﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem; //CRITICAL

public class PlayerInteraction : MonoBehaviour
{
    public InputManager inputManager;
    private bool foundInteractObject;

    private string foundObjTag;
    private Light light;

    public InventoryObject inventory;
    public GameManager gameManager;

    private void Awake()
    {
        inputManager = new InputManager();
        inputManager.Player.Interact.performed += ctx => Interact();
    }

    private void Start()
    {
        foundInteractObject = false;
    }

    private void Interact()
    {
        if (foundInteractObject)
        {
            Debug.Log("Interacted!");

            if (foundObjTag == "Light")
            {
                if (light.enabled == false)
                    light.enabled = true;
                else
                    light.enabled = false;
            }
            else if (foundObjTag == "Bed")
            {
                if (gameManager != null)
                    gameManager.GoToBed();
                else
                    Debug.LogError("GameManager not assigned in PlayerInteraction");
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        foundInteractObject = true;
        foundObjTag = other.tag;

        var item = other.GetComponent<Item>();
        if (item)
        {
            inventory.AddItem(item.item, 1, item.name, item.item.itemMesh);
            Destroy(other.gameObject);
        }
        if (other.tag == "Light") //TODO PROMPT SCRITTO, SCELTA
        { 
                light = other.GetComponentInChildren<Light>();
        }

    }

    private void OnTriggerExit(Collider other)
    {
        foundInteractObject = false;
    }

    private void OnEnable()
    {
        inputManager.Enable();
    }

    private void OnDisable()
    {
        inputManager.Disable();
    }
}
