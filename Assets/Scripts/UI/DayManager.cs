﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DayManager : MonoBehaviour
{
    public int currentDay;
    [SerializeField]
    private TextMeshProUGUI _dayText;

    void Update()
    {
        if (_dayText != null)
        _dayText.text = "Day " + currentDay;
    }

}
