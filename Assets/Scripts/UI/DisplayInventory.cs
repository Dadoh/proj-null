﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;

public class DisplayInventory : MonoBehaviour
{
    public InventoryObject inventory;
    
    public ItemObject itemObject;
    Dictionary<InventorySlot, GameObject> itemsDisplayed = new Dictionary<InventorySlot, GameObject>();

    public InputManager inputManager;

    public bool inventoryOn;

    [SerializeField]
    private GameObject itemModel;
    [SerializeField]
    private GameObject itemName;

    public PlayerMovement player;
    public int invCurrent;

    private Vector3 _originalNamePos;
    private Vector3 _originalModelPos;
    private Vector3 _originalLocalScale;

    public string currentItemName;
    public ItemsUse itemsUseScript;

    // Start is called before the first frame update
    void Start()
    {
        CreateItems();
        inventoryOn = false;

        itemsUseScript = this.GetComponent<ItemsUse>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateItems();
        //Debug.Log(itemModel.transform.position);
        //Debug.Log(itemName.transform.position);
        if (invCurrent < 0)
            invCurrent = 0;
        if (invCurrent > inventory.Container.Count)
            invCurrent--;

        currentItemName = itemsDisplayed[inventory.Container[invCurrent]].GetComponentInChildren<TextMeshProUGUI>().text;
    }

    void Awake()
    {
        //USER INPUTS
        inputManager = new InputManager();
        inputManager.UI.InventoryOpen.performed += ctx => InventoryOpen();
        inputManager.UI.InventoryBack.performed += ctx => InventoryBack();
        inputManager.UI.InventoryForward.performed += ctx => InventoryForward();
        inputManager.UI.InventoryClose.performed += ctx => InventoryClose();
        inputManager.UI.InventoryUse.performed += ctx => itemsUseScript.UseItem();

        _originalNamePos = new Vector3(itemName.transform.position.x, itemName.transform.position.y, itemName.transform.position.z);
        _originalModelPos = new Vector3(itemModel.transform.position.x, itemModel.transform.position.y, itemModel.transform.position.z);
        _originalLocalScale = new Vector3(this.transform.localScale.x, this.transform.localScale.y, this.transform.localScale.z);
    }

    void CreateItems()
    {
        //for (int invCurrent = 0; invCurrent < inventory.Container.Count; invCurrent++)
        //{   if (inventory.Container.Count > 0)
            {
                this.GetComponentInChildren<TextMeshProUGUI>().text = inventory.Container[invCurrent].name;
                this.GetComponentInChildren<MeshFilter>().mesh = inventory.Container[invCurrent].item.prefab.GetComponentInChildren<MeshFilter>().sharedMesh;
            }
        //}
    }

    void UpdateItems()
    { 
        //for (invCurrent = 0; invCurrent < inventory.Container.Count; invCurrent++)
        //{
            if (itemsDisplayed.ContainsKey(inventory.Container[invCurrent]))
            {
                itemsDisplayed[inventory.Container[invCurrent]].GetComponentInChildren<TextMeshProUGUI>().text = inventory.Container[invCurrent].name;
                itemsDisplayed[inventory.Container[invCurrent]].GetComponentInChildren<MeshFilter>().mesh = inventory.Container[invCurrent].item.prefab.GetComponentInChildren<MeshFilter>().sharedMesh;
                
            }
            else
            {
                this.GetComponentInChildren<TextMeshProUGUI>().text = inventory.Container[invCurrent].name;
                this.GetComponentInChildren<MeshFilter>().mesh = inventory.Container[invCurrent].item.prefab.GetComponentInChildren<MeshFilter>().sharedMesh;
                itemsDisplayed.Add(inventory.Container[invCurrent], gameObject);
            }
        //}
    }

    void InventoryOpen()
    {
        if (!inventoryOn)
        {
            Debug.Log("awakened inventory"); //IF ITS NOT EMPTY TODO
            inventoryOn = true;
            this.gameObject.transform.localScale = new Vector3(2, 2, 2);
            
            //itemModel.transform.position = RectTransformUtility.CalculateRelativeRectTransformBounds(GameObject.Find("/Main Camera/InventoryCanvas").transform, itemModel.transform).center;
            itemModel.transform.localPosition = Vector3.zero;
            itemName.transform.localPosition = new Vector3(0, 30, 0);
            if (player != null)
            {
                player.canMove = false;
            }
            
        }
    }

    void InventoryForward() //E //WIP
    {
        if (inventoryOn)
        {
            int prevCount = inventory.Container.Count;
            Debug.Log("inventory fwd");
            if (invCurrent <= inventory.Container.Count)
            {
                invCurrent++;
            }
            else
            {
                Debug.LogError("Inventory slot too high!");
            }
        }
    }

    void InventoryBack() //Q //WIP
    {
        if (inventoryOn)
        {
            Debug.Log("inventory back");
            /*       
            itemsDisplayed[inventory.Container[inventory.Container.Count + 1]].GetComponentInChildren<TextMeshProUGUI>().text = inventory.Container[inventory.Container.Count].name;
            itemsDisplayed[inventory.Container[inventory.Container.Count + 1]].GetComponentInChildren<MeshFilter>().mesh = inventory.Container[inventory.Container.Count].item.prefab.GetComponentInChildren<MeshFilter>().sharedMesh;
            */
            //for (int i = 0; i < inventory.Container.Count; i++)
            //{
                //foreach (KeyValuePair<InventorySlot, GameObject> entry in itemsDisplayed)
                //{
                    if (invCurrent >= 0)
                    {
                        invCurrent--;
                        
                        Debug.Log(invCurrent);
                    }
                    else
                    {
                        Debug.LogError("Inventory Slot too low!");
                        invCurrent = 0;
                    }
                //}
            //}
        }
    }

    void InventoryClose()
    {
        if (inventoryOn == true)
        {
            Debug.Log("inventory close");
            player.canMove = true;
            inventoryOn = false;
            itemModel.transform.position = new Vector3(_originalModelPos.x, _originalModelPos.y, _originalModelPos.z);
            itemName.transform.position = new Vector3(_originalNamePos.x, _originalNamePos.y, _originalNamePos.z);
            this.transform.localScale = new Vector3(_originalLocalScale.x, _originalLocalScale.y, _originalLocalScale.z);
        }
    }

    private void OnEnable()
    {
        inputManager.Enable();
    }

    private void OnDisable()
    {
        inputManager.Disable();
    }
}
