﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneManagerScript : MonoBehaviour
{
    public GameObject[] npc;
    public GameObject dialCanvas;
    // Start is called before the first frame update
    void Start()
    {
        npc = GameObject.FindGameObjectsWithTag("NPC");
        dialCanvas = GameObject.Find("DialogueCanvas");

        if (npc.Length == 0 && dialCanvas != null)
            dialCanvas.SetActive(false);
    }
}
